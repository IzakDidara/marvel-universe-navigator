import React from 'react';
import { Provider } from 'react-redux';
import store from '../core/store';
import NavigationContainer from './components/navigation/NavigationContainer';
import SearchContainer from './components/search/SearchContainer';
import ContentContainer from './components/content/ContentContainer';

const App = () => (
  <Provider store={store}>
    <div className="container">
      <NavigationContainer />
      <SearchContainer />
      <ContentContainer />
    </div>
  </Provider>
);
export default App;
