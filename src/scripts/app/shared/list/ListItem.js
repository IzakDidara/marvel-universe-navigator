import React from 'react';

import './list.scss';

const ListItem = ({ src, name, onClick }) => (
  <div
    className="list__item"
    onClick={onClick}
    role="button"
    tabIndex="0"
  >
    <img className="list__item-image" src={src} alt={name} />
    <div className="list__item-text">{name}</div>
  </div>
);


export default ListItem;
