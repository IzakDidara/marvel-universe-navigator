import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setSearchStatus } from './actions/actions';
import { fetchComics, setActiveComic } from '../content/comics/actions/actions';
import { fetchHeroes, setActiveHero } from '../content/heroes/actions/actions';

import Search from './Search';

const actionCreators = { setSearchStatus, fetchComics, fetchHeroes, setActiveComic, setActiveHero };

function mapStateToProps(state) {
  return {
    search: state.search,
    navigation: state.navigation
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Search);
