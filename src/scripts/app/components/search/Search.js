import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({
      search: e.target.value
    });
  }

  handleSubmit(e) {
    const {
      setSearchStatus, fetchComics, fetchHeroes, navigation,
      setActiveHero, setActiveComic
    } = this.props;

    e.preventDefault();
    setSearchStatus('LOAD');
    setActiveHero(null);
    setActiveComic(null);

    switch (navigation.activeItem.name) {
      case 'Comics':
        return fetchComics(this.state.search);
      case 'Heroes':
        return fetchHeroes(this.state.search);
      default:
        return '';
    }
  }

  render() {
    const { navigation } = this.props;
    const { name } = navigation.activeItem;
    return (
      <div className="search">
        <div className="row">
          <div className="col-lg-10">
            <input
              className="input"
              type="text"
              placeholder={`Please search for ${name.toLowerCase()}`}
              onChange={this.handleChange}
            />
          </div>
          <div className="col-lg-2">
            <button className="button" type="submit" onClick={this.handleSubmit}>Search</button>
          </div>
        </div>
      </div>
    );
  }
}

Search.propTypes = {
  setSearchStatus: PropTypes.func,
  fetchComics: PropTypes.func,
  fetchHeroes: PropTypes.func,
  navigation: PropTypes.shape({
    activeItem: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string
    }),
    items: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string
    }))
  })
};

export default Search;
