function reducer(state = [], action) {
  switch (action.type) {
    case 'SET_SEARCH_STATUS':
      return Object.assign({}, state, { status: action.status });
    default:
      return state;
  }
}

export default reducer;
