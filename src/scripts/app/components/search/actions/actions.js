export function setSearchStatus(status) {
  return {
    type: 'SET_SEARCH_STATUS',
    status
  };
}
