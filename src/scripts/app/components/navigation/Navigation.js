import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './navigation.scss';

class Navigation extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e, navItem) {
    const { setActiveNavigationItem } = this.props;

    e.preventDefault();
    setActiveNavigationItem(navItem);
  }

  render() {
    const { navigation } = this.props;
    return (
      <div className="navigation">
        {navigation.items.map(navItem => (
          <button
            key={navItem.id}
            className={classNames('button button--rect', { 'button--inversed': navItem.id === navigation.activeItem.id })}
            onClick={e => this.handleClick(e, navItem)}
          >
            {navItem.name}
          </button>
        ))}
      </div>
    );
  }
}

Navigation.propTypes = {
  navigation: PropTypes.shape({
    activeItem: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string
    }),
    items: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string
    }))
  }),
  setActiveNavigationItem: PropTypes.func
};

export default Navigation;
