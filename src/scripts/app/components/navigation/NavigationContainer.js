import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setActiveNavigationItem } from './actions/actions';

import Navigation from './Navigation';

const actionCreators = { setActiveNavigationItem };

function mapStateToProps(state) {
  return {
    navigation: state.navigation
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
