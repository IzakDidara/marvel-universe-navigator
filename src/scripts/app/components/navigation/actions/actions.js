export function setActiveNavigationItem(activeItem) {
  return {
    type: 'SET_ACTIVE_NAVIGATION_ITEM',
    activeItem
  };
}

export function setActiveNavigationItemByName(name) {
  return {
    type: 'SET_ACTIVE_NAVIGATION_ITEM_BY_NAME',
    name
  };
}
