function reducer(state = [], action) {
  switch (action.type) {
    case 'SET_ACTIVE_NAVIGATION_ITEM':
      return Object.assign({}, state, { activeItem: action.activeItem });
    case 'SET_ACTIVE_NAVIGATION_ITEM_BY_NAME':
      return Object.assign({}, state, {
        activeItem: state.items.find(item => item.name === action.name)
      });
    default:
      return state;
  }
}

export default reducer;
