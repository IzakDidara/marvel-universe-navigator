import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setActiveHero } from './actions/actions';
import { fetchComic } from '../comics/actions/actions';

import Heroes from './Heroes';

const actionCreators = { setActiveHero, fetchComic };

function mapStateToProps(state) {
  return {
    heroes: state.heroes,
    search: state.search
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Heroes);
