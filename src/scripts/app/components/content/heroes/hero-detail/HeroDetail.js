import React from 'react';

const HeroDetail = ({ src, name, description, onClick, comics }) => (
  <div className="detail">
    <img src={src} alt={name} />
    <h1>{name}</h1>
    <div>{description}</div>
    {comics.items.length !== 0 && <h3>Appeared in comics:</h3>}
    <ul className="detail__list">
      {comics.items.length !== 0 && comics.items.slice(0, 20).map(item => (
        <li key={item.name}>
          <button className="detail__list-button" onClick={e => onClick(e, item)}>
            {item.name}
          </button>
        </li>
      ))}
    </ul>
  </div>
);


export default HeroDetail;
