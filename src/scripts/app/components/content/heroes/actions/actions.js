import api from '../../../../../core/api';
import { setSearchStatus } from '../../../search/actions/actions';
import { setActiveNavigationItemByName } from '../../../navigation/actions/actions';

export function setHeroes(items) {
  return {
    type: 'SET_HEROES',
    items
  };
}

export function setActiveHero(selectedItem) {
  return {
    type: 'SET_ACTIVE_HERO',
    selectedItem
  };
}

export function fetchHeroes(text) {
  return (dispatch) => {
    api.getHeroes(text).then((result) => {
      const heroes = result.data.data.results;
      dispatch(setHeroes(heroes));
      dispatch(setSearchStatus('LOADED'));
    });
  };
}

export function fetchHero(text) {
  return (dispatch) => {
    dispatch(setSearchStatus('LOAD'));
    api.getHero(text).then((result) => {
      const hero = result.data.data.results[0];
      if (hero) {
        dispatch(setActiveHero(hero));
        dispatch(setActiveNavigationItemByName('Heroes'));
      }
      dispatch(setSearchStatus('LOADED'));
    });
  };
}
