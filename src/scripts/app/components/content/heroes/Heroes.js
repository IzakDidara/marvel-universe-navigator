import React, { Component } from 'react';
import HeroDetail from './hero-detail/HeroDetail';
import ListItem from '../../../shared/list/ListItem';


class Heroes extends Component {
  constructor(props) {
    super(props);
    this.handleOpenHero = this.handleOpenHero.bind(this);
    this.handleOpenComic = this.handleOpenComic.bind(this);
  }

  handleOpenHero(e, item) {
    const { setActiveHero } = this.props;

    e.preventDefault();
    setActiveHero(item);
  }

  handleOpenComic(e, item) {
    const { fetchComic } = this.props;
    e.preventDefault();
    fetchComic(item.resourceURI);
  }

  render() {
    const { heroes, search } = this.props;
    if (heroes.selectedItem) {
      const { thumbnail } = heroes.selectedItem;
      const src = `${thumbnail.path}.${thumbnail.extension}`;
      return (
        <HeroDetail src={src} {...heroes.selectedItem} onClick={this.handleOpenComic} />
      );
    }
    return (
      <div className="list">
        {heroes.items && heroes.items.length === 0 && search.status === 'LOADED' ?
          <div className="col-xs-12">Nothing found.</div> : null}
        {heroes.items && heroes.items.map((item) => {
          const src = `${item.thumbnail.path}.${item.thumbnail.extension}`;
          return (
            <ListItem
              src={src}
              key={item.id}
              onClick={e => this.handleOpenHero(e, item)}
              name={item.name}
            />);
        })}
      </div>
    );
  }
}

export default Heroes;
