import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './content.scss';

import HeroesContainer from './heroes/HeroesContainer';
import ComicsContainer from './comics/ComicsContainer';

class Content extends Component {
  constructor(props) {
    super(props);

    this.setSearchContent = this.setSearchContent.bind(this);
    this.getContent = this.getContent.bind(this);
  }

  getContent() {
    const { navigation } = this.props;

    switch (navigation.activeItem.name) {
      case 'Comics':
        return <ComicsContainer />;
      case 'Heroes':
        return <HeroesContainer />;
      default:
        return '';
    }
  }

  setSearchContent() {
    const { search } = this.props;
    switch (search.status) {
      case 'IDLE':
        return '';
      case 'LOAD':
        return <div>Loading ...</div>;
      case 'LOADED':
        return this.getContent();
      default:
        return '';
    }
  }

  render() {
    return (
      <div className="content">
        {this.setSearchContent()}
      </div>
    );
  }
}

Content.propTypes = {
  search: PropTypes.shape({
    status: PropTypes.string
  }),
  navigation: PropTypes.shape({
    activeItem: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string
    }),
    items: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string
    }))
  })
};

export default Content;
