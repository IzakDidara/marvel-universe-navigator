import api from '../../../../../core/api';
import { setSearchStatus } from '../../../search/actions/actions';
import { setActiveNavigationItemByName } from '../../../navigation/actions/actions';

export function setComics(items) {
  return {
    type: 'SET_COMICS',
    items
  };
}

export function setActiveComic(selectedItem) {
  return {
    type: 'SET_ACTIVE_COMIC',
    selectedItem
  };
}

export function fetchComics(text) {
  return (dispatch) => {
    api.getComics(text).then((result) => {
      const comics = result.data.data.results;
      dispatch(setComics(comics));
      dispatch(setSearchStatus('LOADED'));
    });
  };
}

export function fetchComic(text) {
  return (dispatch) => {
    dispatch(setSearchStatus('LOAD'));
    api.getComic(text).then((result) => {
      const comic = result.data.data.results[0];
      if (comic) {
        dispatch(setActiveComic(comic));
        dispatch(setActiveNavigationItemByName('Comics'));
        dispatch(setSearchStatus('LOADED'));
      }
    });
  };
}
