import React, { Component } from 'react';
import ListItem from '../../../shared/list/ListItem';
import ComicDetail from './comic-detail/ComicDetail';

class Comics extends Component {
  constructor(props) {
    super(props);
    this.handleOpenHero = this.handleOpenHero.bind(this);
  }

  handleOpenHero(e, item) {
    const { fetchHero } = this.props;

    e.preventDefault();
    fetchHero(item.resourceURI);
  }

  handleOpenComic(e, item) {
    const { setActiveComic } = this.props;
    e.preventDefault();
    setActiveComic(item);
  }

  render() {
    const { comics, search } = this.props;
    if (comics.selectedItem) {
      const { thumbnail } = comics.selectedItem;
      const src = `${thumbnail.path}.${thumbnail.extension}`;
      return (
        <ComicDetail src={src} {...comics.selectedItem} onClick={this.handleOpenHero} />
      );
    }
    return (
      <div className="list">
        {comics.items && comics.items.length === 0 && search.status === 'LOADED' ?
          <div className="col-xs-12">Nothing found.</div> : null}
        {comics.items && comics.items.map((item) => {
          const src = `${item.thumbnail.path}.${item.thumbnail.extension}`;
          return (
            <ListItem
              src={src}
              key={item.id}
              onClick={e => this.handleOpenComic(e, item)}
              name={item.title}
            />);
        })}
      </div>
    );
  }
}

export default Comics;
