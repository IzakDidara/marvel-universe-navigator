function reducer(state = [], action) {
  switch (action.type) {
    case 'SET_COMICS':
      return Object.assign({}, state, { items: action.items });
    case 'SET_ACTIVE_COMIC':
      return Object.assign({}, state, { selectedItem: action.selectedItem });
    default:
      return state;
  }
}

export default reducer;
