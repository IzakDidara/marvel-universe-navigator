import React from 'react';

const ComicDetail = ({ src, title, description, onClick, characters }) => (
  <div className="detail">
    <img src={src} alt={title} />
    <h1>{title}</h1>
    <div>{description}</div>
    {characters.items.length !== 0 && <h3>Characters Appearing:</h3>}
    <ul className="detail__list">
      {characters.items.length !== 0 && characters.items.slice(0, 20).map(item => (
        <li key={item.name}>
          <button className="detail__list-button" onClick={e => onClick(e, item)}>
            {item.name}
          </button>
        </li>
      ))}
    </ul>
  </div>
);


export default ComicDetail;
