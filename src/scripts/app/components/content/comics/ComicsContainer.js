import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchHero } from '../heroes/actions/actions';
import { setActiveComic } from './actions/actions';

import Comics from './Comics';

const actionCreators = { fetchHero, setActiveComic };

function mapStateToProps(state) {
  return {
    comics: state.comics,
    search: state.search
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Comics);
