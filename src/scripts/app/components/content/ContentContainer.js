import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Content from './Content';

const actionCreators = {};

function mapStateToProps(state) {
  return {
    search: state.search,
    navigation: state.navigation
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Content);
