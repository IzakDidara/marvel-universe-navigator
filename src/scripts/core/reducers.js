import { combineReducers } from 'redux';
import navigationReducer from '../app/components/navigation/reducer/reducer';
import searchReducer from '../app/components/search/reducer/reducer';
import comicsReducer from '../app/components/content/comics/reducer/reducer';
import heroesReducr from '../app/components/content/heroes/reducer/reducer';

export default combineReducers({
  navigation: navigationReducer,
  search: searchReducer,
  comics: comicsReducer,
  heroes: heroesReducr
});
