import axios from 'axios';

const API_KEY = '766195eef174e697baef63c011b202a1';
const path = 'https://gateway.marvel.com:443/v1/public';


export default {
  getHeroes: text => axios.get(`${path}/characters?nameStartsWith=${encodeURIComponent(text)}&apikey=${API_KEY}`),
  getHero: text => axios.get(`${text}?apikey=${API_KEY}`),
  getComics: text => axios.get(`${path}/comics?titleStartsWith=${encodeURIComponent(text)}&apikey=${API_KEY}`),
  getComic: text => axios.get(`${text}?apikey=${API_KEY}`)
};
