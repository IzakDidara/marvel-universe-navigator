import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from './reducers';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middlewares = applyMiddleware(thunk);

const defaultState = {
  navigation: {
    activeItem: { id: 1, name: 'Heroes' },
    items: [
      { id: 1, name: 'Heroes' },
      { id: 2, name: 'Comics' }
    ]
  },
  search: {
    status: 'IDLE'
  },
  heroes: {
    selectedItem: null,
    items: null
  },
  comics: {
    selectedItem: null,
    items: null
  }
};

export default createStore(rootReducer, defaultState, composeEnhancers(middlewares));
