const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

// Locations
const appSrc = ['./src/scripts/index.js', './src/styles/style.scss']; // Paths for the main entry points
const vendorSrc = ['react', './src/styles/vendor.scss']; // From node_modules or specific path

module.exports = {
    entry: {
        app: appSrc, // Entry point for main js and main css/scss
        vendor: vendorSrc, // Use this if you want to split vendors from other js. (create const with array of libraries used)
    },
    output: {
        path: path.resolve(__dirname, 'src/dist'), // Where to output the files
        filename: 'scripts/[name].js', // File name. [name] sets the name provided in entry, in this case it will be app.js, react.js, vendor.js and so on
    },
    module: {
        rules: [
            {
                test: /\.js$/, // regex for finding the right files
                exclude: [ // do not watch those 2 locations, you can also specify a include next to exclude
                    path.resolve(__dirname, 'src/scripts/vendor'),
                    /node_modules/
                ],
                use: [
                    'babel-loader', // used to transpile es6/es2015 to es5. Configuration file is '.babelrc'
                    'eslint-loader', // used for linting your javascript. Configuration file is '.eslintrc'
                ]
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader!resolve-url-loader',
                    publicPath: '/',

                })
            },
            {
                test: /\.(sass|scss)$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'css-loader',
                    use: 'css-loader?sourceMap!resolve-url-loader?sourceMap!postcss-loader?sourceMap!sass-loader?sourceMap&importLoaders=1', // importLoaders=1 fixes problem with autoprefixer not working on imported files(also watch for order)
                    publicPath: '/',

                })
            },
            {
                test: /\.html$/,
                use: ['html-loader']
            },
            {
                test: /\.(jpg|png)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: '[name].[ext]', // To keep the same name use [name] and for same extension use [ext]
                            outputPath: 'dist/images/',
                            publicPath: '/',
                        }
                    }
                ]
            },
            {
                test: /\.(woff2?|ttf|eot|svg)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: '[name].[ext]', // To keep the same name use [name] and for same extension use [ext]
                            outputPath: 'dist/fonts/',
                            publicPath: '/',
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: `dist/styles/[name].css`,
            allChunks: true,
        }),
        new webpack.optimize.CommonsChunkPlugin({ // Split your code into vendor
            name: 'vendor',
            minChunks: Infinity,
        }),
        new HtmlWebpackPlugin({
            inject: true,
            template: 'src/html/index.html', // What file does it take
            filename: 'dist/index.html', // Where and with what name does it export
            chunks: ['app', 'vendor'] // If left empty injects nothing, remove chunks to inject everything
        })
        // new CleanWebpackPlugin(['dist/']), // Used to clean those paths
    ],

    devServer: {
        contentBase: path.join(__dirname, 'src/'),
        compress: true,
        port: 1991,
        historyApiFallback: {
            rewrites: [
                {from: /^\/$/, to: '/dist/index.html'},
            ]
        },
        watchContentBase: true,
    }
};